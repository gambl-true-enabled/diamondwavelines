package com.hetomusy.diamondwave

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger

class RadioApp: Application() {

    override fun onCreate() {
        super.onCreate()
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
                conversionData.forEach {
                    Log.i("RadionConvers", "${it.key} ${it.value}")
                }
            }
            override fun onConversionDataFail(errorMessage: String) {
                Log.i("RadionConvers", "onConversionDataFail $errorMessage")
            }
            override fun onAppOpenAttribution(attributionData: Map<String, String>) {
                attributionData.forEach {
                    Log.i("RadionConvers", "${it.key} ${it.value}")
                }
            }
            override fun onAttributionFailure(errorMessage: String) {
                Log.i("RadionConvers", "onAttributionFailure $errorMessage")
            }
        }
        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this)
    }
}