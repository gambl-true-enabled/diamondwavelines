package com.hetomusy.diamondwave

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class RadioActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ragio)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container, FragmentRadio())
            .commit()

    }
}
