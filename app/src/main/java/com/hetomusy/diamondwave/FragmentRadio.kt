package com.hetomusy.diamondwave

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.radio_fragment.*


class FragmentRadio : Fragment() {

    private var firstPlay = true

    private val radios = listOf(
        RadioDiamond(
            "Record Trancemission",
            "http://air.radiorecord.ru:8102/tm_320",
            R.drawable.radio1
        ),
        RadioDiamond(
            "Record Chill-Out",
            "http://air.radiorecord.ru:8102/chil_320",
            R.drawable.radio2
        ),
        RadioDiamond(
            "Record Гоп FM",
            "http://air.radiorecord.ru:8102/gop_320",
            R.drawable.radio3
        ),
        RadioDiamond(
            "Record Vip Mix",
            "http://air.radiorecord.ru:8102/vip_320",
            R.drawable.radio4
        ),
        RadioDiamond(
            "Record Pirate Station",
            "http://air.radiorecord.ru:8102/ps_320",
            R.drawable.radio5
        ),
        RadioDiamond(
            "Record Deep",
            "http://air.radiorecord.ru:8102/deep_320",
            R.drawable.radio6
        ),
        RadioDiamond(
            "Record Breaks",
            "http://air.radiorecord.ru:8102/brks_320",
            R.drawable.radio7
        )
    )

    private var currentRadioIndex = 0

    private var volume = true

    private var play = false

    private lateinit var mediaPlayer: MediaPlayer

    data class RadioDiamond(
        val name: String,
        val link: String,
        val img: Int
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.radio_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mediaPlayer = MediaPlayer()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mediaPlayer.setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                    .build()
            )
        } else {
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC)
        }

        play_button.setOnClickListener {
            play = if (play) {
                play_button.setImageResource(R.drawable.play_button)
                mediaPlayer.pause()
                false
            } else {
                play_button.setImageResource(R.drawable.pause_button)
                if (firstPlay) {
                    try {
                        mediaPlayer.setOnPreparedListener { player -> player.start() }
                        mediaPlayer.setDataSource(radios[currentRadioIndex].link)
                        mediaPlayer.prepareAsync()
                        firstPlay = false
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    mediaPlayer.start()
                }
                true
            }
        }
        next_button.setOnClickListener {
            currentRadioIndex++
            currentRadioIndex %= 7
            val currentRadio = radios[currentRadioIndex]
            wallpaper_radio.setImageResource(currentRadio.img)
            name_radio.text = currentRadio.name
            play_button.setImageResource(R.drawable.play_button)
            mediaPlayer.stop()
            firstPlay = true
            play = false
            mediaPlayer = MediaPlayer()
        }
        previous_button.setOnClickListener {
            currentRadioIndex--
            if (currentRadioIndex < 0) {
                currentRadioIndex = 6
            }
            val currentRadio = radios[currentRadioIndex]
            wallpaper_radio.setImageResource(currentRadio.img)
            name_radio.text = currentRadio.name
            play_button.setImageResource(R.drawable.play_button)
            mediaPlayer.stop()
            firstPlay = true
            play = false
            mediaPlayer = MediaPlayer()
        }
        val currentRadio = radios[currentRadioIndex]
        wallpaper_radio.setImageResource(currentRadio.img)
        name_radio.text = currentRadio.name


        audio_switch.setOnClickListener {
            volume = if (volume) {
                audio_switch.setImageResource(R.drawable.headphones)
                Toast.makeText(context, "Sound from Headphones", Toast.LENGTH_SHORT).show()
                false
            } else {
                audio_switch.setImageResource(R.drawable.volume)
                Toast.makeText(context, "Sound from Dynamics", Toast.LENGTH_SHORT).show()
                true
            }
        }
    }

}